const { result } = require("underscore");
const invert = require("underscore/cjs/invert");

const ObjectMethods = {


    // Keys


    keys(obj) {
        let result = [];
        if (typeof obj === 'object' && !Array.isArray(obj) && obj !== null) {
            for (let e in obj) {
                result.push(e);
            }
        } else if (Array.isArray(obj)) {
            for (let i = 0; i < obj.length; i++) {
                result.push(i.toString());
            }
        }
        return result;
    },


    // Keys Inbuilt

    keysInbuilt(obj) {
        let result = [];
        if (typeof obj === 'object' && !Array.isArray(obj) && obj !== null) {
            for (let e in obj) {
                result.push(e);
            }
        } else if (Array.isArray(obj) || typeof obj === 'string') {
            for (let i = 0; i < obj.length; i++) {
                result.push(i.toString());
            }
        } else if (obj === null || obj === undefined) {
            result = `TypeError`;
        }
        return result;
    },



    // Values

    values(obj) {
        let result = [];
        if (typeof obj === 'object' && !Array.isArray(obj) && obj !== null) {
            for (let e in obj) {
                if (obj.hasOwnProperty(e) && typeof obj[e] !== 'function'){
                    result.push(obj[e]);
                }
            }
        } else if (Array.isArray(obj)) {
            if (typeof obj === 'string') {
                obj = obj.split('');
            }
            return obj; 
        }
        return result;
    },



    // Values Inbuilt

    valuesInbuilt(obj) {
        let result = [];
        if (typeof obj === 'object' && !Array.isArray(obj) && obj !== null) {
            for (let e in obj) {
                if (obj.hasOwnProperty(e) && typeof obj[e] !== 'function'){
                    result.push(obj[e]);
                }
            }
        } else if (Array.isArray(obj) || typeof obj === 'string') {
            if (typeof obj === 'string') {
                obj = obj.split('');
            }
            return obj; 
        } else if (obj === null || obj === undefined) {
            return `TypeError`
        }
        return result;
    },



    // mapObject

    mapObject(obj, cb) {
        let result = {};
        if (typeof obj === 'object' && !Array.isArray(obj) && obj !== null) {
            result = Object.assign({}, obj);
            for (let e in obj) {
                result[e] = cb(obj[e], e, obj);
            }
        } else if (Array.isArray(obj)) {
            for (let i = 0; i < obj.length; i++) {
                result[i] = cb(obj[i], i, obj)
            }
        }
        return result;
    },



    
    // Pairs


    pairs(obj) {
        let result = [];
        let temp = [];
        if (typeof obj === 'object' && !Array.isArray(obj) && obj !== null) {
            for (let e in obj) {
                temp.push(e);
                temp.push(obj[e]);
                result.push(temp);
                temp = [];
            }
        } else if (Array.isArray(obj)) {
            for (let i = 0; i < obj.length; i++) { 
                temp.push(i.toString());
                temp.push(obj[i]);
                result.push(temp);
                temp = [];
            }
        }
        return result;
    },

    
    

    // Invert

    invert(obj) {
        let result = {};
        if (typeof obj === 'object' && !Array.isArray(obj) && obj !== null) {
            for (let e in obj) {
                result[obj[e]] = e;
            }
        } else if (Array.isArray(obj)) {
            for (let i = 0; i < obj.length; i++) {
                result[obj[i]] = i.toString();
            }
        }
        return result;
    },




    // Defaults

    defaults(obj, props) {
        if (typeof obj === 'object' && !Array.isArray(obj) && obj !== null) {
            
            if (typeof props === 'object' && !Array.isArray(props) && props !== null) {
                for (let e in props) {
                    if (!obj.hasOwnProperty(e)) {
                        obj[e] = props[e];
                    }
                }
                
            } else if (Array.isArray(props)) {
                for (let i = 0; i < props.length; i++) {
                    if (!obj.hasOwnProperty(i)) {
                        obj[i] = props[i];
                    }
                }
            }
            return obj;   
        }
        return {}
        
    },









}

module.exports = ObjectMethods;