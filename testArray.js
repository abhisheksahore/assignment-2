const ArrayObject = require('./arrays');
// console.log(ArrayObject)
const each = require('./node_modules/underscore/cjs/each.js');

const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.
const nestedArray = [1, [2], [], {}, [[3]], [[[4]]]]; // use this to test 'flatten'

// console.log(ArrayObject.flatten(nestedArray));
// console.log(ArrayObject.filter(items, (e, i, a) => e < 4))
// console.log(ArrayObject.find(items, (e, i, a) => e < 4))
console.log(ArrayObject.each(items, (e) => console.log(`foreach ${e}`)))
// console.log(ArrayObject.map(items, (e)=>e*e));
// console.log(ArrayObject.reduce(items, (r, e) => r+e));

let sam = each({a: 1, b: 2}, (e) => console.log(`foreach ${e}`));
console.log(sam)    