const closureObject = {
    // counterFactory


    counterFactory() {
        let x = 0;
        return { 
            increment() {
                return ++x;
            },
            decrement() {
                return --x;
            }
        }
    },
    
    limitFunctionCallCount(cb, n) {
        // Should return a function that invokes `cb`.
        // The returned function should only allow `cb` to be invoked `n` times.
        // Returning null is acceptable if cb can't be returned
        let t = 0;
        return function() {
            if (t < n) {
                t++;
                return cb();
            } else {
                return null;
            }
        }
    },

    cacheFunction(cb) {
        // Should return a funciton that invokes `cb`.
        // A cache (object) should be kept in closure scope.
        // The cache should keep track of all arguments have been used to invoke this function.
        // If the returned function is invoked with arguments that it has already seen
        // then it should return the cached result and not invoke `cb` again.
        // `cb` should only ever be invoked once for a given set of arguments.
        let cache = {};
        return function(x) {
            if (cache[x]) {
                return cache;
            } else {
                cache[x] = 1;
                cb(x);
            }
        }
    } 




}

module.exports = closureObject;