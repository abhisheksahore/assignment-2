
const ArrayObject = {

    // forEach function

    each(elements, cbEach) {

        if (Array.isArray(elements)) {
            if(elements.length > 0){    
                for (let i = 0; i < elements.length; i++) {
                    cbEach(elements[i], i, elements);
                }
                return elements;
            } 
        } else {
            return `each(${typeof elements}) is not a function.`;
        }

    },



    // Map 




    map(elements, cbMap) {
        if(Array.isArray(elements)) {
            if (elements.length > 0) {
                let result = [];
                for (let i = 0; i < elements.length; i++) {
                    result.push(cbMap(elements[i], i, elements));
                }
                return result;
            }
        } else {
            return `map(${typeof elements}) is not a function.`;
        }
    },




    // Reduce



    reduce(elements, cbReduce, startingValue) {
        if (Array.isArray(elements)) {
            if (elements.length > 0) {
                let i = 0;
                if (startingValue === undefined) {
                    startingValue = elements[i];
                    i++;
                }
                for (i; i < elements.length; i++) {
                    startingValue = cbReduce(startingValue, elements[i], i, elements);
                }
                return startingValue;
            } else {
                return `Reduce of empty array with no initial value`;
            } 
        } else {
            return `reduce(${typeof elements}) is not a function.`;
        }
    },



    // Find



    find(elements, cbFind) {
        if (Array.isArray(elements)) {
            if (elements.length > 0) {
                for (let i = 0; i < elements.length; i++) {
                    if (cbFind(elements[i], i, elements)) {
                        return elements[i];
                    } 
                }
            }
        } else {
            return `find(${typeof elements}) is not a function.`;
        }
    },





    // Filter



    filter(elements, cbFilter) {
        if (Array.isArray(elements)) {
            let result = [];
            for (let i = 0; i < elements.length; i++) {
                if (cbFilter(elements[i], i, elements)) {
                    result.push(elements[i])
                }
            }
            return result;
        } else {
            return `filter(${typeof elements}) is not a function.`;
        }
    },



    // Flatten the Array


    flatten(elements) {
        if (Array.isArray(elements)) {
            let result = [];
            if (elements.length > 0) {
                for (let i = 0; i < elements.length; i++) {
                    if (Array.isArray(elements[i])) {
                        result = [...result, ...this.flatten(elements[i])];
                    } else {
                        if (elements[i]) {
                            result.push(elements[i]);
                        }
                    }
                }
            }
            return result;
        } else {
            return `it's not an array`;
        }
    },
}


module.exports = ArrayObject;