const closureObject = require('./closures');

let a = closureObject.counterFactory();
console.log(a.increment());
console.log(a.increment());
console.log(a.increment());
console.log(a.decrement());




let b = closureObject.limitFunctionCallCount(()=>{console.log('ahbishek')}, 3);
b();
b();
b();
b();
b();


let c = closureObject.cacheFunction((i) => {console.log(i)});
c(1);
c(2);
c(3);
c(4);
console.log(c(1));
c(6);
c();